const CHRONO = 30;

const WAIT = 2000;

const STEP = 100;

const ANSWER_CLASSES = 'list-group-item list-group-item-action';
const CORRECT_CLASS = 'list-group-item-success';
const INCORRECT_CLASS = 'list-group-item-danger';

const QUIZ = [
  {
    picture: 'img/image1.jpg',
    question: 'Which of these animals is the strongest ?',
    answers: [
      {answer: 'A hippopotamus', correct: false},
      {answer: 'An elephant', correct: true}
    ]
  },
  {
    picture: 'img/image2.jpg',
    question: 'Which of them is the heaviest ?',
    answers: [
      {answer: 'An apple', correct: false},
      {answer: 'A tank of the USSR', correct: false},
      {answer: 'The developper\'s humor', correct: true}
    ]
  },
];
