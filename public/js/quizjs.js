const DOM = { question, answers, picture, scoring, timer };

let score = 0;
let current = undefined;
let wait = undefined;
let chrono = CHRONO;

/*
* Timer
*/

const ticker = setInterval(tick, 1000);

function tick() {
  if (--chrono < 0) { gameOver(); }
  else { timer.style.width = `${(chrono / CHRONO) * 100}%`; }
}

/*
* Quiz
*/

function nextQuestion() {
  wait = clearTimeout(wait);
  current = QUIZ.splice(Math.floor(Math.random()*QUIZ.length), 1)[0];
  if (current) {
    clearQuestion();
    displayQuestion();
  } else {
    gameOver();
  }
}

function clearQuestion() {
  DOM.answers.querySelectorAll('a').forEach( a => {
    a.removeEventListener('click', checkAnswer);
    DOM.answers.removeChild(a);
  });
}

function displayQuestion() {
  const { question, answers, picture } = DOM;
  // question and picture
  question.innerText = current.question;
  picture.src = current.picture;
  // answers list
  current.answers.forEach( ({answer, correct}) => {
    const a = document.createElement('a');
    a.innerText = answer;
    a.dataset.correct = correct;
    a.classList = ANSWER_CLASSES;
    a.addEventListener('click', checkAnswer);
    answers.appendChild(a);
  });
}

function checkAnswer(event) {
  if (!wait) {
    if (event.target.dataset.correct === 'true') {
      score += STEP;
      DOM.scoring.innerText = score;
    } else {
      event.target.classList.add(INCORRECT_CLASS);
    }
    document.querySelector('[data-correct=true]').classList.add(CORRECT_CLASS);
    wait = setTimeout(nextQuestion, WAIT);
  }
}

function gameOver() {
  DOM.question.innerText = 'Game over !';
  clearInterval(ticker);
  clearQuestion();
}
